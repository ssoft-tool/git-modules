#!/bin/bash
# Usage: complex/clone.sh   <url>
#                           [<directory>]
#                           [-b|--branch <branch>]

ERROR='\033[0;31mERROR\033[0m'
OK='\033[0;32mOK\033[0m'
WARNING='\033[0;33mWARNING\033[0m'
SEPARATOR='\033[1;34m-------\033[0m'

echo -e "${SEPARATOR}"

function prinInfo () {
    echo -e ""
    echo -e "usage: complex/clone.sh [<options>] [--] <repo> [<dir>]"
    echo -e ""
    echo -e "    -b, --branch <branch>     checkout <branch> instead of the remote's HEAD"
    echo -e "    -h, --help                show this help topic"
    echo -e ""
}

POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="${1}"

  case ${key} in
    -b|--branch)
        git_branch="${2}"
        shift # past argument
        shift # past value
        ;;
    -h|--help)
        echo -e "Clones git repository with submodules and checkouts them to actual branches."
        prinInfo
        exit 0
        ;;
    -*)
        echo -e "${ERROR}: Unknown input key ${key}"
        prinInfo
        exit 1
      ;;
    *)    # unknown option
        POSITIONAL+=("${1}") # save it in an array for later
        shift # past argument
        ;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

git_url=${1}
dest_path=${2}

if [ -z "${dest_path}" ]; then
    full_name=${git_url##*/}
    dest_path=${full_name%.*}
fi

echo -e "<url>    = ${git_url}"
echo -e "<dir>    = ${dest_path}"
echo -e "<branch> = ${git_branch}"
echo -e "${OK}: The arguments are parsed."
echo -e "${SEPARATOR}"

# Clone
git clone ${git_url} ${dest_path}
if [[ $? != 0 ]]; then
    echo -e "${ERROR}: Can't clone the project."
    exit 1
fi

cd ${dest_path}
if [[ $? != 0 ]]; then
    echo -e "${ERROR}: The path ${dest_path} not exists."
    exit 1
else
    echo -e "${OK}: The project is cloned successfully."
    echo -e "${SEPARATOR}"
fi

# Checkout
if [ ! -z "${git_branch}" ]; then
    git checkout ${git_branch}
    if [[ $? != 0 ]]; then
        echo -e "${ERROR}: The branch '${git_branch}' not exists."
        echo -e "Use complex/checkout.sh for checkout to existed or new branch."
        exit 1
    else
        echo -e "${OK}: The branch ${git_branch} is checked out successfully."
        echo -e "${SEPARATOR}"
    fi
fi

# Update submodules
git submodule update --init --recursive
if [[ $? != 0 ]]; then
    echo -e "${WARNING}: Can't update some submodules. Check modules avalibility."
    echo -e "Use complex/checkout.sh for checkout to existed or new branch."
    echo -e "Use module/append.sh for add submodule."
    echo -e "Use module/remove.sh for remove submodule."
else
    echo -e "${OK}: The submodules are updated."
    echo -e "${SEPARATOR}"
fi

# Checkout submodules to actual branch
git submodule foreach -q --recursive '\
    submodule_branch=$(git config -f $toplevel/.gitmodules submodule.$name.branch); \
    remote_git=$(git remote); \
    if [ -z "${submodule_branch}" ]; then \
        submodule_branch=$(git remote show ${remote_git} | sed -n '"'"'/HEAD branch/s/.*: //p'"'"'); \
    fi; \
    if [ -z "${submodule_branch}" ]; then \
        echo -e "${WARNING}: The submodule branch is not defined." \
        echo -e "The submodule branch set to master." \
        submodule_branch="master"; \
    fi; \
    git branch -f ${submodule_branch}; \
    git checkout ${submodule_branch}; \
'

if [[ $? != 0 ]]; then
    echo -e "${ERROR}: Some submodules can't be set to last state."
    echo -e "Use git submodule foreach --recursive 'git status' for check status of all submodules."
    echo -e "Use complex/checkout.sh for checkout to existed or new branch."
    echo -e "Use module/append.sh for add submodule."
    echo -e "Use module/remove.sh for remove submodule."
    exit 1
else
    echo -e "${OK}: The branches are set to last state for all submodules."
    echo -e "${SEPARATOR}"
fi
