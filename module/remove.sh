#!/bin/bash
# Usage: module/remove.sh [SRC]

ERROR='\033[0;31mERROR\033[0m'
OK='\033[0;32mOK\033[0m'
WARNING='\033[0;33mWARNING\033[0m'
SEPARATOR='\033[1;34m-------\033[0m'

echo -e -e "${SEPARATOR}"

function prinInfo () {
    echo -e -e ""
    echo -e -e "usage: module/remove.sh [<options>] [--] <dir>"
    echo -e -e ""
    echo -e -e "    -h, --help                show this help topic"
    echo -e -e ""
}

POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="${1}"

  case ${key} in
    -h|--help)
        echo -e -e "Remove git submodule."
        prinInfo
        exit 0
        ;;
    -*)
        echo -e -e "${ERROR}: Unknown input key ${key}"
        prinInfo
        exit 1
      ;;
    *)    # unknown option
        POSITIONAL+=("${1}") # save it in an array for later
        shift # past argument
        ;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

# проверяем, что можно выполнять операции
if [[ `git status --porcelain` ]]; then
    echo -e "${ERROR}: Operation not permitted."
    echo -e "The project has uncommitted changes."
    echo -e "Commit or stash your changes before submodule removing."
    exit 1
fi

name=${1} # requred

if [ -z "${name}" ]; then
    echo -e -e "${ERROR}: The submodule path is not defined."
    exit 1
fi

# Извлекаем путь и названия модуля из .gitmodules
module_path=$(git config -f .gitmodules submodule.${name}.path);

if [ -z "${module_path}" ]; then
    echo -e -e "${ERROR}: The submodule ${name} not exists."
    echo -e -e "Use module/append.sh for append submodule before."
    exit 1
fi

# Проверяем параметры (название и путь)
if [ ! -d ${module_path} ]; then
    echo -e "${ERROR}: Operation not permitted."
    echo -e "The path ${module_path} not exists."
    exit 1
fi

# TODO: Проверяем состояние всех подмодулей на предмет наличия несохраненных изменений.
#       Предлагаем сделать commit.
# TODO: Проверяем состояние всех подмодулей на предмет отправки изменений во внешние репозитории.
#       Предлагаем сделать push/merge request.
# TODO: Если есть неотправленные изменения и нет флага -f --force, то выходим.
#    exit 1


# Если можно спокойно удалять, то
git submodule deinit -f ${module_path}

if [ -d ${module_path} ]; then
    git rm --cached ${module_path}
    git rm ${module_path}
fi

if [ -d ${module_path} ]; then
    rm -rf -f ${module_path}
fi

if [ ! -z "${module_name}" ]; then
    git_module_path=.git/modules/${module_name}

    if [ -d "${git_module_path}" ]; then
        rm -rf ${git_module_path}
    fi
fi

# Удаляем описание модуля из .gitmodules, если не удалился
git commit -m "Remove submodule ${module_path}"