#!/bin/bash
# Usage: module/append.sh   <url>
#                           <directory>
#                           [-b|--branch <branch>]

ERROR='\033[0;31mERROR\033[0m'
OK='\033[0;32mOK\033[0m'
WARNING='\033[0;33mWARNING\033[0m'
SEPARATOR='\033[1;34m-------\033[0m'

echo -e "${SEPARATOR}"

function prinInfo () {
    echo -e ""
    echo -e "usage: module/append.sh [<options>] [--] <repo> <dir>"
    echo -e ""
    echo -e "    -b, --branch <branch>     checkout <branch> instead of the remote's HEAD"
    echo -e "    -h, --help                show this help topic"
    echo -e ""
}

POSITIONAL=()
while [[ $# -gt 0 ]]; do
  key="${1}"

  case ${key} in
    -b|--branch)
        git_branch="${2}"
        shift # past argument
        shift # past value
        ;;
    -h|--help)
        echo -e "Appends git submodule recusively and checkouts all of them to actual branches."
        prinInfo
        exit 0
        ;;
    -*)
        echo -e "${ERROR}: Unknown input key ${key}"
        prinInfo
        exit 1
      ;;
    *)    # unknown option
        POSITIONAL+=("${1}") # save it in an array for later
        shift # past argument
        ;;
  esac
done

set -- "${POSITIONAL[@]}" # restore positional parameters

module_git_url=${1}
module_dest_path=${2}

if [ -z "${module_git_url}" ]; then
    echo -e "${ERROR}: Set the module repo url."
    prinInfo
    exit 1
fi

if [ -z "${module_dest_path}" ]; then
    echo -e "${ERROR}: Set the module destination path."
    prinInfo
    exit 1
fi

git_remote=$(git remote)
if [[ $? != 0 ]]; then
    echo -e "${WARNING}: ..."
fi

complex_git_url=$(git remote get-url ${git_remote})
if [[ $? != 0 ]]; then
    echo -e "${WARNING}: ..."
    exit 1
fi

# Заменяем абсолютный путь module_git_url на относительный (при возможности)
source_path=${complex_git_url}
target_path=${module_git_url}
common_part=${source_path}
result_path=""

# Заменяем символ ':' на '/', если url начинается с git@
# чтобы обеспечить верную работу функции dirname
if [[ ${common_part} == git@* ]] && [[ ${target_path} == git@* ]]; then
    common_part=${common_part/":"/"/"}
    target_path=${target_path/":"/"/"}
fi

# Заменяем символы "://" на "_"
# чтобы обеспечить верную работу функции dirname
common_part=${common_part/"://"/"_"}
target_path=${target_path/"://"/"_"}

# Определяем общую часть common_part
while [[ "${target_path#${common_part}}" == "${target_path}" ]] && [[ ${common_part} != "." ]]; do
    common_part="$(dirname ${common_part})"
    if [[ -z ${result_path} ]]; then
        result_path=".."
    else
        result_path="../${result_path}"
    fi
done

# Определяем окончание forward_part от target_path
forward_part="${target_path#${common_part}}"

# Определяем результат, как относительный или абсолютный
if [[ -n ${result_path} ]] && [[ -n ${forward_part} ]] && [[ ${forward_part} != ${target_path} ]]; then
    result_path="${result_path}${forward_part}"
elif [[ -n ${forward_part} ]]; then
    result_path="${forward_part}"
fi

# Если результат относительный, то заменяем исходный url
if [ "${result_path}" != "${forward_part}" ]; then
    module_git_url=${result_path}
fi

git_name=${module_dest_path}

echo -e "<url>    = ${module_git_url}"
echo -e "<dir>    = ${module_dest_path}"
echo -e "<name>   = ${git_name}"
echo -e "<branch> = ${git_branch}"
echo -e "${OK}: The arguments are parsed."
echo -e "${SEPARATOR}"

# проверяем, что можно выполнять операции
if [[ `git status --porcelain` ]]; then
    echo -e "${ERROR}: Operation not permitted."
    echo -e "The project has uncommitted changes."
    echo -e "Commit or stash your changes before submodule appending."
    exit 1
fi

# проверяем отсутствие модуля и пути в .gitmodules
test_path=$(git config -f .gitmodules submodule.${name}.path);
if [ ! -z "${test_path}" ]; then
    echo -e "${ERROR}: The submodule ${name} exists already."
    echo -e "Use module/remove.sh for remove submodule."
    exit 1
fi

# выполняем операцию добавления
if [ -z "${git_branch}" ]; then
    git submodule add --name ${git_name} --force ${module_git_url} ${module_dest_path}
else
    git submodule add --name ${git_name} --force --branch ${git_branch} ${module_git_url} ${module_dest_path}
fi

git commit -m "The submodule '${git_name}' is appended from '${module_git_url}' to '${module_dest_path}'."

# Подгружаем все подмодули
cd ${module_dest_path}

# Update submodules
git submodule update --init --recursive
if [[ $? != 0 ]]; then
    echo -e "${WARNING}: Can't update some submodules. Check modules avalibility."
    echo -e "Use complex/checkout.sh for checkout to existed or new branch."
    echo -e "Use module/append.sh for add submodule."
    echo -e "Use module/remove.sh for remove submodule."
else
    echo -e "${OK}: The submodules are updated."
    echo -e "${SEPARATOR}"
fi

# Checkout submodules to actual branch
git submodule foreach -q --recursive '\
    submodule_branch=$(git config -f $toplevel/.gitmodules submodule.$name.branch); \
    remote_git=$(git remote)
    if [ -z "${submodule_branch}" ]; then \
        submodule_branch=$(git remote show ${remote_git} | sed -n '"'"'/HEAD branch/s/.*: //p'"'"'); \
    fi; \
    if [ -z "${submodule_branch}" ]; then \
        echo -e "${WARNING}: The submodule branch is not defined." \
        echo -e "The submodule branch set to master." \
        submodule_branch="master"; \
    fi; \
    git branch -f ${submodule_branch}; \
    git checkout ${submodule_branch}; \
'

if [[ $? != 0 ]]; then
    echo -e "${ERROR}: Some submodules can't be set to last state."
    echo -e "Use git submodule foreach --recursive 'git status' for check status of all submodules."
    echo -e "Use complex/checkout.sh for checkout to existed or new branch."
    echo -e "Use module/append.sh for add submodule."
    echo -e "Use module/remove.sh for remove submodule."
    exit 1
else
    echo -e "${OK}: The branches are set to last state for all submodules."
    echo -e "${SEPARATOR}"
fi
